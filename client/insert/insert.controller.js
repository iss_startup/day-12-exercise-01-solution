(function () {
    angular
        .module("DepartmentApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http"];

    function InsertCtrl($http) {
        var vm = this;

        vm.department = {};
        vm.status = {
            message: "",
            success: ""
        };

        // Exposed functions
        vm.register = register;

        // Function definition
        function register() {

            $http.post("/api/departments", vm.department)
                .then(function (response) {
                    console.info("Success! Response returned: " + JSON.stringify(response.data));
                    vm.status.message = "The department is added to the database.";
                    vm.status.success = "ok";
                    console.info("status: " + JSON.stringify(vm.status));
                })
                .catch(function (err) {
                    console.info("Error: " + err);
                    vm.status.message = "Failed to add the department to the database.";
                    vm.status.success = "error";
                });
        }
    } // END InsertCtrl

})();






