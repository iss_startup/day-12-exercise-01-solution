//Load dependencies
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory  __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));

//TODO :Create a connection with mysql DB <HERE>
var MYSQL_USERNAME = 'root';
var MYSQL_PASSWORD = 'root';
var conn = new Sequelize('employees', MYSQL_USERNAME, MYSQL_PASSWORD, {});

var Department = require('./models/department')(conn, Sequelize);
var Manager = require('./models/manager')(conn, Sequelize);
var Employee = require('./models/employee')(conn, Sequelize);


// Associations
Department.hasMany(Manager, {foreignKey: 'dept_no'});
Manager.hasOne(Employee, {foreignKey: 'emp_no'});


// For exercise: delete, update, and searchByID; also includes API naming exercise;
// uses named parameters and req.body
// -- Searches for specific department by dept_no
app.get("/api/departments/:dept_no", function (req, res) {
    console.log("-- GET /api/departments/:dept_no");
    console.log("-- GET /api/departments/:dept_no request params: " + JSON.stringify(req.params));

    var where = {};
    if (req.params.dept_no) {
        where.dept_no = req.params.dept_no
    }

    // We use findOne because we know (by looking at the database schema) that dept_no is the primary key and
    // is therefore unique. We cannot use findById because findById does not support eager loading
    Department
        .findOne({
            where: where
            // What Include attribute does: Join two or more tables. In this instance:
            // 1. For every Department record that matches the where condition, the include attribute returns
            // ALL employees that have served as managers of said Department
            // 2. model attribute specifies which model to join with primary model
            // 3. order attribute specifies that the list of Managers be ordered from latest to earliest manager
            // 4. limit attribute specifies that only 1 record (in this case the latest manager) should be returned
            , include: [{
                model: Manager
                , order: [["to_date", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
                , include: [Employee]
            }]
        })
        // this .then() handles successful findAll operation
        // in this example, findAll() used the callback function to return departments
        // we named it departments, but this object also contains info about the
        // latest manager of that department
        .then(function (departments) {
            console.log("-- GET /api/departments/:dept_no findOne then() result \n " + JSON.stringify(departments));
            res.json(departments);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("-- GET /api/departments/:dept_no findOne catch() \n " + JSON.stringify(departments));
            res
                .status(500)
                .json({error: true});
        });

});
// -- Updates department info
app.put('/api/departments/:dept_no', function (req, res) {
    console.log("-- PUT /api/departments/:dept_no");
    console.log("-- PUT /api/departments/:dept_no request params: " + JSON.stringify(req.params));
    console.log("-- PUT /api/departments/:dept_no request body: " + JSON.stringify(req.body));

    var where = {};
    where.dept_no = req.params.dept_no;

    // Updates department detail
    Department
        .update(
            {dept_name: req.body.dept_name}             // assign new values
            , {where: where}                            // search condition / criteria
        )
        .then(function (response) {
            console.log("-- PUT /api/departments/:dept_no department.update then(): \n"
                + JSON.stringify(response));
        })
        .catch(function (err) {
            console.log("-- PUT /api/departments/:dept_no department.update catch(): \n"
                + JSON.stringify(err));
        });
    // Keeping this for reference
    // Department.find({
    //         where: {
    //             dept_no: req.body.dept_no
    //         }
    //     })
    //     .then(function (response) {
    //         response.updateAttributes(
    //             {
    //                 dept_name: req.body.dept_name
    //             })
    //             .then(function (response) {
    //                 console.log(response);
    //             })
    //             .catch(function (error) {
    //                 console.log(error);
    //             })
    //     })
});
// -- Searches for and deletes manager of a specific department.
app.delete("/api/managers/:dept_no/:emp_no", function (req, res) {
    console.log("-- DELETE /api/managers/:dept_no/:emp_no");
    console.log("-- DELETE /api/managers/:dept_no/:emp_no request params: " + JSON.stringify(req.params));
    var where = {};
    where.dept_no = req.params.dept_no;
    where.emp_no = req.params.emp_no;
    console.log("where " + JSON.stringify(where));

    // The dept_manager table's primary key is a composite of dept_no and emp_no
    // We will use these to find our manager. It is important to include dept_no because an employee maybe a
    // manager of 2 or more departments. Even if business logic doesn't support this, always search
    // a table and delete rows of a table based on the defined primary keys
    Manager
        .destroy({
            where: where
        })
        .then(function (result) {
            console.log("-- DELETE /api/managers/:dept_no/:emp_no then(): \n" + JSON.stringify(result));
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});

        })
        .catch(function (err) {
            console.log("-- DELETE /api/managers/:dept_no/:emp_no catch(): \n" + JSON.stringify(err));
        });
});


// For exercise: insert, find all, and eager loading;
// uses query strings and req.body
app.post("/api/departments", function (req, res) {
    console.log("Query", req.query);
    console.log("Body", req.body);

    Department
        .create({
            dept_no: req.body.dept_no,
            dept_name: req.body.dept_name
        })
        .then(function (department) {
            res
                .status(200)
                .json(department);
        })
        .catch(function (err) {
            console.log("error: " + err);
            res
                .status(500)
                .json({error: true});
        })


});
app.get("/api/departments", function (req, res) {
    var where = {};

    if (req.query.dept_name) {
        where.dept_name = {
            $like: "%" + req.query.dept_name + "%"
        }
    }

    if (req.query.dept_no) {
        where.dept_no = req.query.dept_no;
    }

    console.log("where: " + JSON.stringify(where));
    Department
        .findAll({
            where: where
        })
        .then(function (departments) {
            res.json(departments);
        })
        .catch(function () {
            res
                .status(500)
                .json({error: true})
        });

});
app.get("/api/managers", function (req, res) {
    var where = {};

    console.log("query: " + JSON.stringify(req.query));
    if (req.query.dept_name) {
        where.dept_name = {
            $like: "%" + req.query.dept_name + "%"
        }
    }
    Department
        .findAll({
            where: where
            // What Include attribute does: Join two or more tables. In this instance:
            // 1. For every Department record that matches the where condition, the include attribute returns
            // ALL employees that have served as managers of said Department
            // 2. model attribute specifies which model to join with primary model
            // 3. order attribute specifies that the list of Managers be ordered from latest to earliest manager
            // 4. limit attribute specifies that only 1 record (in this case the latest manager) should be returned
            , include: [{
                model: Manager
                , order: [["to_date", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
                , include: [Employee]
            }]
        })
        // this .then() handles successful findAll operation
        // in this example, findAll() used the callback function to return departments
        // we named it departments, but this object also contains info about the
        // latest manager of that department
        .then(function (departments) {
            console.log("departments " + JSON.stringify(departments));
            res.json(departments);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("error " + err);
            res
                .status(500)
                .json({error: true});
        });
});


//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});